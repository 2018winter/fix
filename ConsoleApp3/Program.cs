﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;
using System.IO;
using QuickFix.Fields;
using System.Threading;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Establish FIX Server Connection
            SessionSettings settings = new SessionSettings("C:\\Users\\Li\\Desktop\\fix\\ConsoleApp3\\sessionSettings.txt");
            CallMessage application = new CallMessage();
            FileStoreFactory fileStoreFactory = new FileStoreFactory(settings);
            FileLogFactory logFactory = new FileLogFactory(settings);
            IMessageFactory messageFactory = new DefaultMessageFactory();
            QuickFix.Transport.SocketInitiator initiator = new QuickFix.Transport.SocketInitiator(application, fileStoreFactory, settings, logFactory, messageFactory);
            initiator.Start();
            #endregion  
            
            File.Delete("C:\\Users\\Li\\Desktop\\fix\\MFM.csv");
            File.Delete("C:\\Users\\Li\\Desktop\\fix\\CWP.csv");
            File.Delete("C:\\Users\\Li\\Desktop\\fix\\Return.csv");

            Thread.Sleep(1000 * 5);
            Plot GUI = new Plot();
            Portfolio pfo = new Portfolio
            {
                application.MFM,
                application.CWP
            };
            pfo.SetAUM(10000);
            DateTime TimeStart = new DateTime();
            TimeStart = DateTime.Now;
            TimeSpan Timer = new TimeSpan();
            //set the time span for the duration of the trading session
            TimeSpan TargetDuration = new TimeSpan(0, 15, 0);
            int TimeIndi = -1;

            Strategy strategy = new Strategy(application.Mysession.SessionID);
            //System.Windows.Forms.Application.Run(GUI);

            while (application.Mysession != null && TimeIndi == -1)
            {
                //double absdiff = Math.Abs(application.mfmp - application.cwpp);
                if (application.TradeSignal > 0.6)
                {
                    strategy.FrontRun(application.MFMLog[6], pfo, application.Mysession.SessionID,100);
                    pfo.UpdateAUM(application.TTC);
                }
                else if(application.TradeSignal > 0.3)
                {
                    strategy.FrontRun(application.MFMLog[6], pfo, application.Mysession.SessionID, 10);
                    pfo.UpdateAUM(application.TTC);
                }
                else
                {
                    if(pfo[1].Position != 0)
                    {
                        pfo[1].Liquidall(application.Mysession.SessionID);
                        pfo.UpdateAUM(application.TTC);
                    }
                    
                }

                pfo.PnL();
                Console.WriteLine(string.Format("\nP/L: {0:0.00}", pfo.CurrentPnl));
                Console.WriteLine(string.Format("CASH: {0:0.00}",pfo.AUM));
                Console.WriteLine("MFM: "+ pfo[0].Position);
                Console.WriteLine("CWP: " + pfo[1].Position);
                Console.WriteLine(application.TradeSignal);
                pfo.Record(pfo.CurrentPnl, application.cwpp, application.TradeSignal, pfo.AUM);

                Timer = application.TimerEnd - TimeStart;
                TimeIndi = AnalyzeData.Timestop(Timer, TargetDuration);
                //pfo.StopTrading(initiator);
                Thread.Sleep(250);
            }

            initiator.Stop();
            Console.WriteLine("Session Ended");



        }

    }
}
