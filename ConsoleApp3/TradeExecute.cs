﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;
using System.IO;
using QuickFix.Fields;
using System.Threading;

namespace ConsoleApp3
{
    /// <summary>
    /// Class used to send trade to the server
    /// </summary>
    class TradeExecute
    {
        
        SessionID mysessionID;
        private string ordid;
        private string symbol;
        private char tradeside;
        private int quantity;
        /// <summary>
        /// trade message builder. for trade side use '1' as buy, '2' as sell 
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="ticker"></param>
        /// <param name="bos"></param>
        /// <param name="qty"></param>
        public TradeExecute(SessionID currentsession ,string oid, string ticker, char bos, int qty)
        {
            ordid = oid;
            symbol = ticker;
            tradeside = bos;
            quantity = qty;
            mysessionID = currentsession;
        }
        /// <summary>
        /// Send trade to the server
        /// </summary>
        public void Trade()
        {
            QuickFix.FIX44.NewOrderSingle order = new QuickFix.FIX44.NewOrderSingle(
                new ClOrdID(this.ordid),
                new Symbol(this.symbol),
                new Side(this.tradeside),
                new TransactTime(DateTime.Now),
                new OrdType(OrdType.MARKET)
                );
            order.OrderQty = new OrderQty(new decimal(quantity));
            Session.SendToTarget(order, mysessionID);
        }

    }
}
