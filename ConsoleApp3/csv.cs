﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp3
{
    public class CsvWrite
    {
        public static void Write(string path, string v1, string v2, string v3)
        {
            if (!File.Exists(path))
            {
                File.WriteAllText(path, " ");
            }
            else
            {
                string appendText = string.Format("{0}, {1}, {2}" + Environment.NewLine, v1, v2, v3);
                File.AppendAllText(path, appendText);
            }          
        }

        public static void Write(string path, string v1, string v2, string v3, string v4)
        {
            if (!File.Exists(path))
            {
                File.WriteAllText(path, " ");
            }
            else
            {
                string appendText = string.Format("{0}, {1}, {2}, {3}" + Environment.NewLine, v1, v2, v3, v4);
                File.AppendAllText(path, appendText);
            }
        }
    }
}
