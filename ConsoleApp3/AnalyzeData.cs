﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using MathNet.Numerics.Statistics;


namespace ConsoleApp3
{
    class AnalyzeData
    {
        public static void Collect(List<double> list ,double element, int noftickes)
        {
            if (list.Count < noftickes)
            {
                list.Add(element);
            }
            else
            {
                list.RemoveAt(0);
                list.Add(element);
            }
        }

        public static void Collect(List<double> list, double element)
        {
            list.Add(element);
        }

        public static double Sharpe(List<double> preturn, double riskfree)
        {
            double ratio = 0;
            ratio = (preturn.Average() - riskfree) / Statistics.StandardDeviation(preturn);
            return ratio;
        }

        public static double CalcReturn(double p1, double p2)
        {
            double ratio = 0;
            ratio = (p2 - p1) / p1;
            return ratio;
        }

        public static double Corre(IEnumerable<double> list1, IEnumerable<double> list2)
        {
            double correlation = Correlation.Pearson(list1, list2);   
            return correlation;
        }

        public static int Timestop(TimeSpan t1, TimeSpan t2)
        {
            int indi = t1.CompareTo(t2);
            return indi;
        }

        public static double TradeCondition(double p1, double p2)
        {
            double ratio = 0;
            ratio = Math.Abs(p1 - p2) * 2 / (p1 + p2);
            return ratio;
        }

        public static List<double> LogReturn(List<double> list)
        {
            List<double> log = new List<double>();
            for(int i = 1; i<list.Count(); i++)
            {
                double lr = Math.Log(list[i] / list[i - 1]);
                log.Add(lr);
            }
            return log;
        }
    }
}
