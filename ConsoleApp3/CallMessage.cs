﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;
using QuickFix.Fields;
using System.Threading;

namespace ConsoleApp3
{
    class CallMessage : MessageCracker, IApplication
    {
        #region override application method
        public void OnLogon(SessionID sessionID)
        {
            Console.Write("Logon " + sessionID);
            Mysession = Session.LookupSession(sessionID);

        }
        public void OnLogout(SessionID sessionID)
        {
            Console.Write("\nLogout " + sessionID);
            Mysession = null;
        }
        public void ToAdmin(Message message, SessionID sessionID)
        {
        }
        public void FromAdmin(Message message, SessionID sessionID)
        {
        }
        public void ToApp(Message message, SessionID sessionId)
        {

        }
        public void OnCreate(SessionID sessionID)
        {
            Console.Write("Session Created\n");
        }
        #endregion
        #region Variables
        public Equity MFM = new Equity("MFM", 0, 0);
        public Equity CWP = new Equity("CWP", 0, 0); 
        public Session Mysession = null;
        public DateTime TimerEnd = new DateTime();
        public List<double> MFMList = new List<double>();
        public List<double> CWPList = new List<double>();
        public double mfmp;
        public double cwpp;
        double MFMTradeCost;
        double CWPTradeCost;
        public double TTC;
        public double TradeSignal;
        public List<double> MFMLog = new List<double>();
        public List<double> CWPLog = new List<double>();
        #endregion
        public void FromApp(Message message, SessionID sessionID)
        {
            TimerEnd = DateTime.Now;
            if (message.Header.GetString(35) == "8")
            {
                string tticker = message.GetString(55);
                int tqty = message.GetInt(14);
                char tradeside = message.GetChar(54);
                double tprice = Convert.ToDouble(message.GetDecimal(44));

                if (tticker == "MFM")
                {
                    MFM.PositionChange(tradeside, tqty);
                    MFMTradeCost = MFM.TradeCost(tradeside, tprice, tqty);

                    //MFM.PositionValue();
                }
                else if (tticker == "CWP")
                {
                    CWP.PositionChange(tradeside, tqty);
                    CWPTradeCost = MFM.TradeCost(tradeside, tprice, tqty);
                    //CWP.PositionValue()
;                }
                TTC = CWPTradeCost + MFMTradeCost;
            }

            double price = Convert.ToDouble(message.GetDecimal(133));
            string ticker = message.GetString(117);
            DateTime time = message.Header.GetDateTime(52);
            if (ticker == "MFM")
            {
                CsvWrite.Write("C:\\Users\\Li\\Desktop\\fix\\MFM.csv", ticker, price.ToString(), time.ToString());
                AnalyzeData.Collect(MFMList, price, 11);
                mfmp = price;
                MFM.CurrentPrice = price;
                MFM.PositionValue();
            }
            else
            {
                CsvWrite.Write("C:\\Users\\Li\\Desktop\\fix\\CWP.csv", ticker, price.ToString(), time.ToString());
                AnalyzeData.Collect(CWPList, price, 11);
                cwpp = price;
                CWP.CurrentPrice = price;
                CWP.PositionValue();
            }

            if (MFMList.Count() >= 11 && CWPList.Count() >= 11)
            {
                MFMLog = AnalyzeData.LogReturn(MFMList);
                CWPLog = AnalyzeData.LogReturn(CWPList);
                double[] MFM5before = new double[5];
                double[] CWP5after = new double[5];
                MFMLog.CopyTo(0, MFM5before, 0, 5);
                CWPLog.CopyTo(5, CWP5after, 0, 5);
                TradeSignal = AnalyzeData.Corre(MFM5before, CWP5after);
            }
        }

    }
}