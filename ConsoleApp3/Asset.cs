﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;

namespace ConsoleApp3
{
    class Asset
    {
        public string Name { get; set; }
        public int Position { get; set; }
        public double CurrentPrice {get;set;}
        public double Value { get; set; }

        public void PositionChange(char side, int change)
        {
            if(side == '1')
            {
                Position += change;
            }
            else
            {
                Position -= change;
            }
        }
        public double TradeCost(char side, double unitprice, int tradqty)
        {
            double cost = 0;
            if(side == '1')
            {
                cost = unitprice * tradqty;
            }
            else
            {
                cost = unitprice * tradqty * -1;
            }
            return cost;
        }
        public void PositionValue()
        {
            Value = 0;
            Value = CurrentPrice * Position;
        }
        public void Buy(SessionID sess, int qty)
        {
            TradeExecute Buy = new TradeExecute(sess, "1", Name, '1', qty);
            Buy.Trade();
        }
        public void Sell(SessionID sess, int qty)
        {
            TradeExecute Sell = new TradeExecute(sess, "1", Name, '2', qty);
            Sell.Trade();
        }
        public void Liquidall(SessionID session)
        {
            if (Position > 0)
            {
                Sell(session, Position);
            }
            else if (Position < 0)
            {
                Buy(session, Position*-1);
            }
            else
            {
                Console.WriteLine("not executed");
            }
        }

    }
    class Equity : Asset
    {
        public Equity(string sname, int sqty, double cprice)
        {
            Name = sname;
            Position = sqty;
            CurrentPrice = cprice;
        }
    }
}
