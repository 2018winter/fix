﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;

namespace ConsoleApp3
{
    public partial class Plot : Form
    {
        public Plot()
        {
            InitializeComponent();
            var model = new PlotModel { Title = "this is an example" };
            model.Series.Add(new FunctionSeries(Math.Cos, 0, 10, 0.1,"cos(x)"));
            plotView1.Model = model;
        }
    }
}
