﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickFix;

namespace ConsoleApp3
{
    class Strategy
    {
        protected SessionID NewSessionID { get; set; }

        public Strategy(SessionID nsession)
        {
            NewSessionID = nsession;
        }

        public void Simple(double MFMp, double CWPp)
        {
            TradeExecute BuyMFM = new TradeExecute(NewSessionID, "1", "MFM", '1', 1);
            TradeExecute SellCWP = new TradeExecute(NewSessionID, "1", "CWP", '2', 1);
            TradeExecute BuyCWP = new TradeExecute(NewSessionID, "1", "CWP", '1', 1);
            TradeExecute SellMFM = new TradeExecute(NewSessionID, "1", "MFM", '2', 1);

            if (MFMp > CWPp)
            {
                SellMFM.Trade();
                BuyCWP.Trade();
            }
            else
            {
                BuyMFM.Trade();
                SellCWP.Trade();
            }

        }

        public void FrontRun(double MFMLog, Portfolio portfolio, SessionID session, int qty)
        {
            if (MFMLog > 1)
            {
                portfolio[1].Buy(session, qty);
            }
            else
            {
                portfolio[1].Sell(session, qty);
            }
        }
    }
}
