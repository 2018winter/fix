﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.Statistics;
using QuickFix.Transport;
using OxyPlot;
using QuickFix;

namespace ConsoleApp3
{
    class Portfolio: List<Asset>
    {
        public List<Asset> AssetList{ get; set; }
        public double AUM { get; set; }
        public double InitialCap { get; set; }
        public List<double> PnLLog = new List<double>();
        public double CurrentPnl;

        public void SetAUM (double InitialAUM)
        {
            AUM = InitialAUM;
            InitialCap = InitialAUM;
        }

        public double GetValue()
        {
            double TotalValue = 0;
            foreach (Asset a in this)
            {
                TotalValue += a.Value;
            }
            return TotalValue;
        }

        public void PnL()
        {
            double CurrentPValue = GetValue();
            CurrentPnl = CurrentPValue + AUM - InitialCap;
            AnalyzeData.Collect(PnLLog, CurrentPnl);         
        }

        public void Record(double p1, double p2, double tradesignal, double pnl)
        {
            CsvWrite.Write("C:\\Users\\Li\\Desktop\\fix\\Return.csv", pnl.ToString(), p1.ToString(), p2.ToString(), tradesignal.ToString());
        }

        public void UpdateAUM(double cost)
        {
            AUM -= cost;
            //CostLog.Add(cost);
        }

        public void StopTrading(SocketInitiator initiator)
        {
            if(AUM/InitialCap < 0.98)
            {
                initiator.Stop();
                Console.WriteLine("Reached Maximum Loss");
            }
        }

        public void LiquidAll(SessionID session)
        {
            foreach (Asset a in this)
            {
                if(a.Position != 0)
                {
                    a.Liquidall(session);
                }
            }
        }


    }
}
