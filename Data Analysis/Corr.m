function [ result ] = Corr( Data )

MFM = Data(:,1);
CWP = Data(:,2);

cor = corrcoef(MFM,CWP);

result = cor;


end

