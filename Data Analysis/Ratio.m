function [ result ] = Ratio( Data )

MFM = Data(:,1);
CWP = Data(:,2);

r1 = MFM./CWP;
r2 = CWP./MFM;

plot(r1)
hold on
plot(r2)


end

